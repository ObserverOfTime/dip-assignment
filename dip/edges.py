from typing import Tuple

import cv2 as cv
import numpy as np


def sobel(img: np.ndarray, kernel: Tuple[int, int] = (7, 7),
          ksize: int = 1) -> np.ndarray:
    """
    Detect edges using Sobel derivates.

    See https://docs.opencv.org/3.4/d2/d2c/tutorial_sobel_derivatives.html.

    :param img: The input image.
    :type img: np.ndarray
    :param kernel: The kernel used in ``cv.GaussianBlur``.
    :type kernel: Tuple[int, int]
    :param ksize: The size of the extended Sobel kernel.
    :type ksize: int
    :return: The output edge map.
    :rtype: np.ndarray
    """
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray, kernel, 0)
    grad_x = cv.Sobel(blur, cv.CV_16S, 1, 0, ksize=ksize)
    grad_y = cv.Sobel(blur, cv.CV_16S, 0, 1, ksize=ksize)
    return cv.addWeighted(
        cv.convertScaleAbs(grad_x), 0.5,
        cv.convertScaleAbs(grad_y), 0.5, 0
    )


def laplace(img: np.ndarray, kernel: Tuple[int, int] = (7, 7),
            ksize: int = 1) -> np.ndarray:
    """
    Detect edges using the Laplace operator.

    See https://docs.opencv.org/3.4/d5/db5/tutorial_laplace_operator.html.

    :param img: The input image.
    :type img: np.ndarray
    :param kernel: The kernel used in ``cv.GaussianBlur``.
    :type kernel: Tuple[int, int]
    :param ksize: Aperture size used to compute the second-derivative filters.
    :type ksize: int
    :return: The output edge map.
    :rtype: np.ndarray
    """
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray, kernel, 0)
    edges = cv.Laplacian(blur, cv.CV_16S, ksize)
    return cv.convertScaleAbs(edges)


def canny(img: np.ndarray, thresholds: Tuple[int, int] = (125, 250),
          kernel: Tuple[int, int] = (5, 5), ksize: int = 3) -> np.ndarray:
    """
    Detect edges using the Canny edge detector.

    See https://docs.opencv.org/3.4/da/d5c/tutorial_canny_detector.html.

    :param img: The input image.
    :type img: np.ndarray
    :param thresholds: The thresholds for the hysteresis procedure.
    :type thresholds: Tuple[int, int]
    :param kernel: The kernel used in ``cv.GaussianBlur``.
    :type kernel: Tuple[int, int]
    :param ksize: The aperture size for the Sobel operator.
    :type ksize: int
    :return: The output edge map.
    :rtype: np.ndarray
    """
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray, kernel, 0)
    return cv.Canny(blur, thresholds[0], thresholds[1], ksize)


__all__ = ['sobel', 'laplace', 'canny']
