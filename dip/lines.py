from typing import Tuple

import cv2 as cv
import numpy as np

from dip.edges import canny


def hough(img: np.ndarray, dist: int = 1, angle: float = 28.8,
          threshold: int = 150, length: int = 225,
          gap: int = 25) -> Tuple[np.ndarray, int]:
    """
    Detect line segments using the probabilistic Hough transform.

    See https://docs.opencv.org/3.4/d9/db0/tutorial_hough_lines.html.

    :param img: 8-bit, single-channel binary source image.
    :type img: np.ndarray
    :param dist: Distance resolution of the accumulator in pixels.
    :type dist: int
    :param angle: Angle resolution of the accumulator in degrees.
    :type angle: float
    :param threshold: Accumulator threshold.
    :type threshold: int
    :param length: Minimum line length.
    :type length: int
    :param gap: Maximum allowed gap between points on the same line.
    :type gap: int
    :return: The output line map (lines in red) and the number of lines.
    :rtype: Tuple[np.ndarray, int]
    """
    canvas = img.copy()
    edges = canny(img)
    lines = cv.HoughLinesP(
        edges, dist, np.pi / 180 * (angle + 1),
        threshold, minLineLength=length, maxLineGap=gap
    )
    for line in lines:
        l1, l2, l3, l4 = line[0]
        cv.line(canvas, (l1, l2), (l3, l4), (0, 0, 255), 2)
    return canvas, len(lines)


def rotate(img: np.ndarray, angle: float = 28.8) -> np.ndarray:
    """
    Rotate an image by a given angle.

    :param img: The original image.
    :type img: np.ndarray
    :param angle: The rotation angle.
    :type angle: float
    :return: The rotated image.
    :rtype: np.ndarray
    """
    rows, cols = img.shape[:2]
    rot = cv.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)
    return cv.warpAffine(img, rot, (cols, rows))


def rectangle(img: np.ndarray, thresholds:
              Tuple[int, int] = (127, 178)) -> np.ndarray:
    """
    Find the largest rectangle in a given image.

    :param img: The source image.
    :type img: np.ndarray
    :param threshold: The color thresholds used in ``cv.threshold``.
    :type threshold: Tuple[int, int]
    :return: The output image with the rectangle drawn in blue.
    :rtype: np.ndarray
    """
    canvas = img.copy()
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    thresh = cv.threshold(
        gray, thresholds[0], thresholds[1], cv.THRESH_BINARY
    )[1]
    contours = cv.findContours(
        thresh, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE
    )[1]
    # cv.drawContours(canvas, contours, -1, (0, 0, 255), 2)
    cnt = max(contours, key=cv.contourArea)
    x, y, w, h = cv.boundingRect(cnt)
    cv.rectangle(canvas, (x, y), (x + w, y + h), (255, 0, 0), 2)
    return canvas


__all__ = ['hough', 'rotate', 'rectangle']
