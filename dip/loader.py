from pathlib import Path
from typing import Tuple

import cv2 as cv
import numpy as np

#: The directory where the images are stored.
DATA_DIR: Path = Path(__file__).resolve().parents[1] / 'data'

cols: Tuple[str, ...] = (
    '982', '985', '987', '990', '992',
    '995', '997', '000', '002'
)

rows: Tuple[str, ...] = (
    '232', '230', '227', '225', '222',
    '220', '217', '215', '212', '210'
)


def load_img(col: str, row: str) -> np.ndarray:
    """
    Load an image from the data directory.

    :param col: The column of the image.
    :type col: str
    :param row: The row of the image.
    :type row: str
    :return: The loaded image.
    :rtype: np.ndarray
    """
    return cv.imread(str(DATA_DIR / '{}{}.jp2'.format(col, row)))


def downsample(img: np.ndarray, ratio: float) -> np.ndarray:
    """
    Downsample an image to the specified ratio.

    :param img: The image to be downsampled.
    :type img: np.ndarray
    :param ratio: The downsampling ratio.
    :type ratio: float
    :return: The image after downsampling.
    :rtype: np.ndarray
    """
    return cv.resize(
        img, None, fx=ratio, fy=ratio,
        interpolation=cv.INTER_AREA
    )


def stitch(ds_ratio: float = 0.025) -> np.ndarray:
    """
    Stitch all the images into one.

    :param ds_ratio: The downsampling ratio.
    :type ds_ratio: float
    :return: The stitched image.
    :rtype: np.ndarray
    """
    return np.vstack([
        np.hstack([
            downsample(load_img(c, r), ds_ratio) for c in cols
        ]) for r in rows
    ])


__all__ = ['DATA_DIR', 'load_img', 'downsample', 'stitch']
