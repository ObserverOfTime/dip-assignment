#!/usr/bin/env python3

import cv2 as cv

from dip.edges import canny
from dip.lines import hough, rectangle, rotate
from dip.loader import DATA_DIR, stitch

# Q1, Q2, Q3
ny = DATA_DIR.with_name('out') / 'new-york.jp2'
if not ny.exists():
    img = stitch()
    cv.imwrite(str(ny), img)
else:
    img = cv.imread(str(ny))

# Q5
edges = ny.with_name('edges.jp2')
eimg = canny(img)
cv.imwrite(str(edges), eimg)

# Q6
lines = ny.with_name('lines.jp2')
limg, num = hough(img)
cv.imwrite(str(lines), limg)
print('Found {} lines in the original image.'.format(num))

# Q7
rotated = ny.with_name('rotated.jp2')
rimg = rotate(img)
cv.imwrite(str(rotated), rimg)

# Q8
rlines = ny.with_name('rotated-lines.jp2')
rlimg, num = hough(rimg)
cv.imwrite(str(rlines), rlimg)
print('Found {} lines in the rotated image.'.format(num))

# Q9
park = ny.with_name('central-park.jp2')
pimg = rectangle(rimg)
cv.imwrite(str(park), pimg)
