Digital Image Processing Assignment
===================================

Install `pipenv <https://docs.pipenv.org/en/latest/install/>`_

Clone the project:

.. code:: sh

   git clone https://gitlab.com/ObserverOfTime/dip-assignment
   cd dip-assignment

Place the images in the ``data/`` directory.

Install the requirements:

.. code:: sh

   pipenv update --dev

Run ``flake8`` to check the coding style:

.. code:: sh

   pipenv run flake8 dip/

Run ``main.py`` to execute the assignment:

.. code:: sh

   pipenv run main
