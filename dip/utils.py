from typing import Optional, Tuple

import cv2 as cv
import numpy as np


def imwindow(title: str, img: np.ndarray,
             size: Optional[Tuple[int, int]] = None) -> None:
    """
    Create a resizable image window.

    :param title: The title of the window.
    :type title: str
    :param img: The image to display.
    :type img: np.ndarray
    :param size: The original size of the window.
    :type size: Optional[Tuple[int, int]]
    """
    cv.namedWindow(title, cv.WINDOW_NORMAL)
    if size is not None:
        cv.resizeWindow(title, size)
    cv.imshow(title, img)
    while True:
        key = cv.waitKey(100)
        if key == 27:
            break  # pressed ESC
        if cv.getWindowProperty(title, 4) < 1:
            break  # closed window
    cv.destroyWindow(title)


__all__ = ['imwindow']
